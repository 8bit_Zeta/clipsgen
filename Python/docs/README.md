# Clipsgen Python Documentation

## Variables

# templateList
This is a list that can be used to store the created templates.

# factList
This is a list that can be used to store the created facts.

# ruleList
This is a list that can be used to store the created rules.

## Functions

# createTemplate(self, name, list)
Takes in a name for the template along with a list of names for the accompanying slots.
Returns a CLIPS template string using said template name and slot names.

# createFact(self, template, list)
Takes in the name of the template to be associated with along with a list of names for the accompanying slots.
Returns a CLIPS fact string using said template name and slot names.

# createRule(self, name, conditionList, resultList)
Takes in a name for the rule along with a list of conditions and results that the rule will consist of.
Returns a CLIPS rule string using said rule name, conditions, and results.

# createComparison(self, comparison, conditions)
Takes in a comparitor along with a list of conditions.
Returns a comparison string to be used in a CLIPS rule using said comparitor and conditions.

# createIf(self, eq, list)
Takes in an equality attribute along with a list of actors.
Returns a CLIPS If string using said equality attribute and actors.

# createAssertion(self, list)
Takes in an equality attribute along with a list of actors.
Returns a CLIPS If string using said equality attribute and actors.
